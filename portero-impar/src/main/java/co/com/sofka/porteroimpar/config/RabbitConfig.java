package co.com.sofka.porteroimpar.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    Queue oneZeroOneQueue() {
        return new Queue("oneZeroOneQueue", false);
    }

    @Bean
    Queue threeZeroOneQueue() {
        return new Queue("threeZeroOneQueue", false);
    }

    @Bean
    Queue sixZeroTwoQueue() {
        return new Queue("sixZeroTwoQueue", false);
    }

    @Bean
    Queue unpairFloorQueue() {
        return new Queue("unpairFloorQueue", false);
    }

    @Bean
    TopicExchange topicExchange() {
        return new TopicExchange("topic-exchange");
    }

    @Bean
    Binding oneZeroOneBinding(Queue oneZeroOneQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(oneZeroOneQueue).to(topicExchange).with("unpair.one.zero.one");
    }

    @Bean
    Binding threeZeroOneBinding(Queue threeZeroOneQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(threeZeroOneQueue).to(topicExchange).with("unpair.three.zero.one");
    }

    @Bean
    Binding sixCeroTwoBinding(Queue sixZeroTwoQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(sixZeroTwoQueue).to(topicExchange).with("pair.six.zero.two");
    }

    @Bean
    Binding unpairFloorBinding(Queue unpairFloorQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(unpairFloorQueue).to(topicExchange).with("unpair.*");
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    MessageListenerContainer messageListenerContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
        simpleMessageListenerContainer.setConnectionFactory(connectionFactory);
        return simpleMessageListenerContainer;
    }

    public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
}
