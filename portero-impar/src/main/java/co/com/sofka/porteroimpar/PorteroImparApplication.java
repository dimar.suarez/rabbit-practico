package co.com.sofka.porteroimpar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PorteroImparApplication {

	public static void main(String[] args) {
		SpringApplication.run(PorteroImparApplication.class, args);
	}

}
