package co.com.sofka.porterotopic.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    Queue zeroZeroOneQueue() {
        return new Queue("zeroZeroOneQueue", false);
    }

    @Bean
    Queue sixZeroOneQueue() {
        return new Queue("sixZeroOneQueue", false);
    }

    @Bean
    Queue sixZeroTwoQueue() {
        return new Queue("sixZeroTwoQueue", false);
    }

    @Bean
    Queue sixthFloorQueue() {
        return new Queue("sixthFloorQueue", false);
    }

    @Bean
    TopicExchange topicExchange() {
        return new TopicExchange("topic-exchange");
    }

    @Bean
    Binding zeroZeroOneBinding(Queue zeroZeroOneQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(zeroZeroOneQueue).to(topicExchange).with("zero.zero.one");
    }

    @Bean
    Binding sixZeroOneBinding(Queue sixZeroOneQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(sixZeroOneQueue).to(topicExchange).with("six.zero.one");
    }

    @Bean
    Binding sixCeroTwoBinding(Queue sixZeroTwoQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(sixZeroTwoQueue).to(topicExchange).with("six.zero.two");
    }

    @Bean
    Binding sixthFloorBinding(Queue sixthFloorQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(sixthFloorQueue).to(topicExchange).with("six.zero.*");
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    MessageListenerContainer messageListenerContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
        simpleMessageListenerContainer.setConnectionFactory(connectionFactory);
        return simpleMessageListenerContainer;
    }

    public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
}
