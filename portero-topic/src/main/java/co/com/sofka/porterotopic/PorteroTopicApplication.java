package co.com.sofka.porterotopic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PorteroTopicApplication {

	public static void main(String[] args) {
		SpringApplication.run(PorteroTopicApplication.class, args);
	}

}
